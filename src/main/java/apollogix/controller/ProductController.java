package apollogix.controller;

import apollogix.ecommerce.product.Product;
import apollogix.ecommerce.product.application.IProductApplication;
import apollogix.ecommerce.product.command.CommandSearchProduct;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/api")
public class ProductController extends AbstractController {
    @Autowired
    private IProductApplication productApplication;

    @GetMapping("/get_product/{productId}")
    public Product getProducts(@PathVariable String productId) {
        if (StringUtils.isBlank(productId))
            return null;
        return productApplication.getProductById(productId).orElse(null);
    }

    @PostMapping("/search_product")
    public List<Product> getProducts(@RequestBody CommandSearchProduct command,
                                     @RequestParam(required = false, defaultValue = "1") Integer page,
                                     @RequestParam(required = false, defaultValue = "1") Integer size) {
        command.setPage(page);
        command.setSize(size);
        return productApplication.searchProducts(command).orElse(new ArrayList<>());
    }
}
