package apollogix.controller;

import apollogix.ecommerce.order.Order;
import apollogix.ecommerce.order.application.IOrderApplication;
import apollogix.ecommerce.order.command.CommandAddToCart;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api")
public class OrderController extends AbstractController {
    @Autowired
    private IOrderApplication orderApplication;

    @PostMapping("/add_to_cart")
    public Order addToCart(@RequestBody CommandAddToCart command) throws Exception {
        return orderApplication.addToCart(command).orElse(null);
    }

    @GetMapping("/get_orders")
    public List<Order> getAllOrder() throws Exception {
        return orderApplication.getAllOrder().orElse(null);
    }

}
