package apollogix.ecommerce.product.repository;

import apollogix.data_demo.ProductDataDemo;
import apollogix.ecommerce.product.Product;
import org.apache.commons.collections4.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;

@Component
public class ProductRepository implements IProductRepository {
    @Autowired
    private ProductDataDemo productDataDemo;

    @Override
    public Optional<Product> getOne(String productId) {
        //todo: query database
        List<Product> products = productDataDemo.getProducts();
        return products.stream().filter(f -> f.getProductId().equals(productId)).findFirst();
    }

    @Override
    public Optional<List<Product>> getProducts(Map<String, Object> query, int page, int size) {
        //todo: query database
        List<Product> products = productDataDemo.getProducts().stream()
                .filter(f -> query.get("productId") == null
                        || f.getProductId().equals(String.valueOf(query.get("productId"))))
                .filter(f -> query.get("name") == null
                        || f.getName().equals(String.valueOf(query.get("name"))))
                .filter(f -> query.get("color") == null
                        || f.getMultipleColors().stream().anyMatch(f1 -> f1.getColor().equals(String.valueOf(query.get("color")))))
                .filter(f -> query.get("category") == null
                        || f.getMultipleCategories().stream().anyMatch(f1 -> f1.getCategory().equals(String.valueOf(query.get("category")))))
                .filter(f -> query.get("price") == null
                        || f.getPrice().equals(query.get("price")))
                .collect(Collectors.toList());
        if (CollectionUtils.isEmpty(products))
            return Optional.of(new ArrayList<>());
        return Optional.of(products.subList((page - 1) * size, Math.min((page - 1) * size + size, products.size())));
    }
}
