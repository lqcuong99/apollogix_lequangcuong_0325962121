package apollogix.ecommerce.product.repository;

import apollogix.ecommerce.product.Product;

import java.util.List;
import java.util.Map;
import java.util.Optional;

public interface IProductRepository {
    Optional<Product> getOne(String productId);

    Optional<List<Product>> getProducts(Map<String, Object> query, int page, int size);
}
