package apollogix.ecommerce.product.command;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommandSearchProduct {
    private String productId;
    private String productName;
    private Double price;
    private String color;
    private String category;
    @Builder.Default
    private int page = 1;
    @Builder.Default
    private int size = 10;
}
