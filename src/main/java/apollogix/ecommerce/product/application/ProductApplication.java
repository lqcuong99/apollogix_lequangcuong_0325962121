package apollogix.ecommerce.product.application;

import apollogix.ecommerce.product.Product;
import apollogix.ecommerce.product.command.CommandSearchProduct;
import apollogix.ecommerce.product.repository.IProductRepository;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

@Component
public class ProductApplication implements IProductApplication {
    @Autowired
    private IProductRepository productRepository;

    @Override
    public Optional<Product> getProductById(String productId) {
        if (StringUtils.isBlank(productId))
            return Optional.empty();
        //todo: filter here (if need)
        return productRepository.getOne(productId);
    }

    @Override
    public Optional<List<Product>> searchProducts(CommandSearchProduct command) {
        //todo: filter here (if need)
        Map<String, Object> query = new HashMap<>();
        if (StringUtils.isNotBlank(command.getProductId()))
            query.put("productId", command.getProductId().trim());
        if (StringUtils.isNotBlank(command.getProductName()))
            query.put("name", command.getProductName().trim());
        if (command.getPrice() != null)
            query.put("price", command.getPrice());
        if (StringUtils.isNotBlank(command.getColor()))
            query.put("color", command.getColor().trim());
        if (StringUtils.isNotBlank(command.getCategory()))
            query.put("category", command.getCategory().trim());
        return productRepository.getProducts(query, command.getPage(), command.getSize());
    }
}
