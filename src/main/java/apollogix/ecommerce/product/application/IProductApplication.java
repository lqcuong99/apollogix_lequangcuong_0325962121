package apollogix.ecommerce.product.application;

import apollogix.ecommerce.product.Product;
import apollogix.ecommerce.product.command.CommandSearchProduct;

import java.util.List;
import java.util.Optional;

public interface IProductApplication {
    Optional<Product> getProductById(String productId);

    Optional<List<Product>> searchProducts(CommandSearchProduct command);
}
