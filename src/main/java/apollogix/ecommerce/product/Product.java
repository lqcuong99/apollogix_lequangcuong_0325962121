package apollogix.ecommerce.product;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Product {
    private String productId;
    private String name;
    private String description;
    private String image;
    private Double price;
    private List<Color> multipleColors;
    private List<Category> multipleCategories;

    @Data
    @Builder
    public static class Color {
        private int colorId;
        private String color;
    }

    @Data
    @Builder
    public static class Category {
        private int categoryId;
        private String category;
    }
}
