package apollogix.ecommerce.order;

import apollogix.ecommerce.product.Product;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
@Builder
public class Order {
    private String orderId;
    @Builder.Default
    private List<ProductInformation> productInCard = new ArrayList<>();

    //todo: extra fields(if need)
//    private String customerName;
//    ...
    @Data
    @Builder
    public static class ProductInformation {
        private Product product;
        private Double quantity;
        private Double totalPrice;
    }
}
