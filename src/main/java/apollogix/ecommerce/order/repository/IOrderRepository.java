package apollogix.ecommerce.order.repository;

import apollogix.ecommerce.order.Order;

import java.util.List;
import java.util.Optional;

public interface IOrderRepository {
    Optional<Order> insertOne(Order order);

    Optional<Order> getOne(String orderId);

    Optional<List<Order>> getMany();
}
