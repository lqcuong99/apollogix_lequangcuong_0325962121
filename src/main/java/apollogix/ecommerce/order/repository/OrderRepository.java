package apollogix.ecommerce.order.repository;

import apollogix.data_demo.OrderStoreDemo;
import apollogix.ecommerce.order.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;

@Component
public class OrderRepository implements IOrderRepository {
    @Autowired
    private OrderStoreDemo orderStoreDemo;

    @Override
    public Optional<Order> insertOne(Order order) {
        orderStoreDemo.getOrders().removeIf(f -> f.getOrderId().equals(order.getOrderId()));
        orderStoreDemo.getOrders().add(order);
        return Optional.of(order);
    }

    @Override
    public Optional<Order> getOne(String orderId) {
        return orderStoreDemo.getOrders().stream().filter(f -> f.getOrderId().equals(orderId)).findFirst();
    }

    @Override
    public Optional<List<Order>> getMany() {
        return Optional.of(orderStoreDemo.getOrders());
    }
}
