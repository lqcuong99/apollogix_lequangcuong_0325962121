package apollogix.ecommerce.order.command;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class CommandAddToCart {
    private String orderId;
    private String productId;
    private Double quantity;
}
