package apollogix.ecommerce.order.application;

import apollogix.ecommerce.order.Order;
import apollogix.ecommerce.order.command.CommandAddToCart;

import java.util.List;
import java.util.Optional;

public interface IOrderApplication {
    Optional<Order> addToCart(CommandAddToCart command) throws Exception;

    Optional<List<Order>> getAllOrder() throws Exception;
}
