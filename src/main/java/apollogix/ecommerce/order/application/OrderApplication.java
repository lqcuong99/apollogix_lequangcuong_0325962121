package apollogix.ecommerce.order.application;

import apollogix.ecommerce.order.Order;
import apollogix.ecommerce.order.command.CommandAddToCart;
import apollogix.ecommerce.order.repository.IOrderRepository;
import apollogix.ecommerce.product.Product;
import apollogix.ecommerce.product.application.IProductApplication;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@Component
public class OrderApplication implements IOrderApplication {
    @Autowired
    private IOrderRepository orderRepository;
    @Autowired
    private IProductApplication productApplication;

    @Override
    public Optional<Order> addToCart(CommandAddToCart command) throws Exception {
        if (StringUtils.isBlank(command.getProductId()))
            throw new Exception("productId is empty");
        if (command.getQuantity() == null || command.getQuantity() <= 0)
            throw new Exception("quantity must than 0");
        Order order = Order.builder().build();
        if (StringUtils.isNotBlank(command.getOrderId())) {
            Optional<Order> orderOptional = orderRepository.getOne(command.getOrderId());
            if (!orderOptional.isPresent())
                throw new Exception("orderId is not exist");
            order = orderOptional.get();
        } else {
            order.setOrderId(UUID.randomUUID().toString());
        }
        Optional<Product> productOptional = productApplication.getProductById(command.getProductId());
        if (!productOptional.isPresent())
            throw new Exception("productId is not exist");
        Optional<Order.ProductInformation> productInCartOptional = order.getProductInCard().stream()
                .filter(f -> f.getProduct().getProductId().equals(command.getProductId())).findFirst();
        if (productInCartOptional.isPresent()) {
            productInCartOptional.get().setQuantity(productInCartOptional.get().getQuantity() + command.getQuantity());
            productInCartOptional.get().setTotalPrice(productOptional.get().getPrice() * productInCartOptional.get().getQuantity());
        } else {
            order.getProductInCard().add(Order.ProductInformation.builder()
                    .product(productOptional.get())
                    .quantity(command.getQuantity())
                    .totalPrice(productOptional.get().getPrice() * command.getQuantity())
                    .build());
        }
        //todo: check quantity in stock, expire date...
        return orderRepository.insertOne(order);
    }

    @Override
    public Optional<List<Order>> getAllOrder() throws Exception {
        return orderRepository.getMany();
    }
}
