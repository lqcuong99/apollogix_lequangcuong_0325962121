package apollogix;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ApollogixApplication {

    public static void main(String[] args) {
        SpringApplication.run(ApollogixApplication.class, args);
    }

}
