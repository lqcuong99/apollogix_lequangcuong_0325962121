package apollogix.data_demo;

import apollogix.ecommerce.product.Product;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class ProductDataDemo {
    private final List<Product> products;

    @Autowired
    ProductDataDemo(ResourceLoader resourceLoader, ObjectMapper objectMapper) {
        try {
            Resource resource = resourceLoader.getResource("classpath:data_temp/product_data.json");
            try (InputStream inputStream = resource.getInputStream()) {
                products = objectMapper.readValue(inputStream, new TypeReference<List<Product>>() {
                });
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Product> getProducts() {
        return products;
    }
}
