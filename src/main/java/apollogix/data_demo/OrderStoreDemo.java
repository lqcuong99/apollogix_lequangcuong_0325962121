package apollogix.data_demo;

import apollogix.ecommerce.order.Order;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.io.InputStream;
import java.util.List;

@Component
public class OrderStoreDemo {
    private final List<Order> orders;

    @Autowired
    OrderStoreDemo(ResourceLoader resourceLoader, ObjectMapper objectMapper) {
        try {
            Resource resource = resourceLoader.getResource("classpath:data_temp/order_data.json");
            try (InputStream inputStream = resource.getInputStream()) {
                orders = objectMapper.readValue(inputStream, new TypeReference<List<Order>>() {
                });
            }
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public List<Order> getOrders() {
        return orders;
    }
}
