***QUAN TRỌNG:
	Link download file: https://drive.google.com/file/d/1-aHDJYDRA8PUfBwhNhkobiJW_LTaTiap/view?usp=drive_link
	Link gitlab: https://gitlab.com/lqcuong99/apollogix_lequangcuong_0325962121 
    Để chạy được ứng dụng, anh/chị cài trước giúp em java version 8/cấu hình biến môi trường.
***

Sau khi giải nén ra được folder "apollogix", truy cập vào folder "apollogix" vừa giải nén được để tiếp tục:
	1. Mở Command Prompt, nhập:
		mvn clean install

	2. Di chuyển tới thư mục chưa file jar:
		cd target/

	3. Run file api (Apollogix-0.0.1-SNAPSHOT.jar)
		java -jar Apollogix-0.0.1-SNAPSHOT.jar

Đợi ứng dụng chạy hoàn tất, call api từ postman. List api được liệt kê bên dưới:
	1. Get product bằng productId
	curl --location 'localhost:8080/api/get_product/711f1453-e3f2-4714-a531-9118abf819c2'

	2. Search product theo điều kiện
	curl --location 'localhost:8080/api/search_product?page=1&size=1' \
	--header 'Content-Type: application/json' \
	--data '{
		"productId": null,
		"productName": null,
		"price": 58.0,
		"color": null,
		"category": null
	}'

	3. Thêm sản phẩm vào giỏ hàng
	curl --location 'localhost:8080/api/add_to_cart' \
	--header 'Content-Type: application/json' \
	--data '{
		"orderId": "order_01",
		"productId": "416bcb20-d3b4-4e0d-bc4e-927a871d6ada",
		"quantity": 1.0
	}'

	4. Get tất cả giỏ hàng
	curl --location 'localhost:8080/api/get_orders'


***NOTE:
	1. Nếu muốn đổi port -> vào apollogix\src\main\resources\application.properties
		server.port=8080 -> đổi 8080 thành port mong muốn

	2. Thư mục apollogix\src\main\resources\data_temp sẽ chưa 2 file json
		2.1. order_data.json
			File này chứa thông tin giỏ hàng tạm, giả lập như database
		2.2. product_data.json
			File này giả lập thông tin sản phẩm
	3. Nếu chạy như trên báo lỗi anh/chị chạy api này bằng IDE(Intellij, Eclipse) giúp em.
***

Contact:
	Name: Le Quang Cuong
	Mail: lqcuong2121@gmail.com
	Phone: 0325962121

CẢM ƠN ANH/CHỊ